"use strict";
(function() {

  $(document).ready(function(){
    var activePage = "";

    $("#listLink").click(function(){
      if (!activePage  || activePage === "create") {
        activePage = "list";
        $("#content").fadeOut(500);
      //  $("#content")..load('../reservation-list.html').fadeIn(700);

        $('#content').delay(400).queue(function( nxt ) {
          $("#content").load('../reservation-list.html').fadeIn(500);
          nxt();
        });
      }
    });

    $("#createLink").click(function(){
      if (!activePage  || activePage === "list") {
        activePage = "create";
        $("#content").fadeOut(500);
      //  $("#content").delay(500).load('../reservation.html').fadeIn(700);

        $('#content').delay(500).queue(function( nxt ) {
            $("#content").delay(400).load('../reservation.html').fadeIn(500);
            nxt();
        });

      }
    });




  });

})();
