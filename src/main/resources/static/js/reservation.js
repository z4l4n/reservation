/**
* Script to handle placing reservations.
*/

"use strict";

(function() {

	$(document).ready(function() {


		function clearFormInput() {
			$("input").val("");
		}

		function collectFormInput() {

			var reservationFormValues = {};
			$.each($('#reservationForm').serializeArray(), function(i, field) {
				reservationFormValues[field.name] = field.value;
			});

			return reservationFormValues;
		}

		// throws exception
		// todo date-from-to check
		// I could do this with handlebars template too, but I have no time :D
		function validateFormInputs(inputs) {
			var res = "";

			if (!inputs.resourceId.length) {
				res += "<br/><div>The facility ID can't be empty!</div>";
			}

			if (!(/^\d+$/.test(inputs.resourceId)) || inputs.resourceId <= 0) {
				res += "<br/><div>The facility ID must be a non negative number!</div>";
			}

			if (!inputs.fromDate.length) {
				res += "<br/><div>The start date can't be empty!</div>";
			}

			if (!inputs.toDate.length) {
				res += "<br/><div>The end date can't be empty!</div>";
			}

			if (inputs.fromDate.length && inputs.toDate.length) {
				var d1 = Date.parse(inputs.toDate);
				var d2 = Date.parse(inputs.fromDate);
				if (d1 < d2) {
					res += "<br/><div>The end date can't be smaller than the start date!</div>";
				}
			}

			if (!inputs.owner.length) {
				res += "<br/><div>The owner can't be empty!</div>";
			}

			if (res.length) {
				throw res;
			}

		}

		$("#submitReservation").click(function(e) {
			e.preventDefault();

			$('.message').hide();

			var reservationRequest = collectFormInput();
			try {
				validateFormInputs(reservationRequest);


				var reservationUrl =
				'/api/reservations/' + encodeURIComponent(reservationRequest.resourceId)
				+ '/from-' + encodeURIComponent(reservationRequest.fromDate)
				+ '/to-' + encodeURIComponent(reservationRequest.toDate)
				+ '/?owner=' + encodeURIComponent(reservationRequest.owner);

				$.ajax({
					url : reservationUrl,
					method: 'POST',
					async : true,
					cache : false,
					timeout : 5000,

					data : {},

					success : function(data, statusText, response) {
						var reservationId = response.getResponseHeader('reservation-id');
						$('#reservation-successful-message').append(" " + reservationId);
						$('#reservation-successful-message').fadeIn(2000).delay(3000).fadeOut(2000);
						clearFormInput();
					},

					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log("reservation request failed ... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);

						var errorCodeToHtmlIdMap = {400 : '#validation-error', 405 : '#validation-error', 409 : '#conflict-error' , 500: '#system-error'};
						var id = errorCodeToHtmlIdMap[XMLHttpRequest.status];

						if (!id) {
							id =  errorCodeToHtmlIdMap[500];
						}

						$(id).fadeIn(1000).delay(4000).fadeOut(1000);

					}
				});

				// input validation can throw exception
			} catch(exc) {
				console.log("exc ág!");
				console.log(exc);
				$('#UIValidation-error').html(exc);
				$('#UIValidation-error').fadeIn(1000).delay(4000).fadeOut(1000);
			}

		});
	});

})();
